import serial
import logging
from time import sleep

# Set version
__version__ = '0.1.0-beta1'

# create logger
logger = logging.getLogger(__name__)


class PHD2000Error(Exception):
    """
    PHD2000 exception handle
    """

    def __init__(self, err, command):
        self.logger = logging.getLogger(__name__)
        self.errdict = {'?': "Unrecognized command.",
                        '*': "Pump stalled.",
                        'OOR': 'Value out of range.'}
        self.errStr = self.errdict[err] + ' Command was "{}"'.format(command)
        self.logger.error(self.errStr)
        return

    def __str__(self):
        return self.errStr

    '''Python SDK for the Harvard PHD 2000

    Here comes the description. This is followed by the definition of the input parameters and usually a nice example.

    :param comPort: address of serial port
    :type comPort: string

    Example::
        import phd2000
        import logging
        
        port = '/dev/ttyUSB0'
        
        # Create logger
        logging.basicConfig(level=logging.DEBUG, filename='example.log',
                            format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        
        with phd2000.Device(port) as pump:
            version = pump.sendrcv('VER')
            # pump.sendrcv('MLM5.1')
            diametre = pump.sendrcv('DIA')
            pump.sendrcv('MMD30.0')
        pass

    '''


class Device(object):
    def __init__(self, comport, rate=9600):
        """ Device class for Harvard PHD 2000 syringe pump

        :param comport: address of serial port
        :param rate: baudrate
        """
        self.__version__ = __version__
        self.logger = logging.getLogger(__name__)
        self.comLink = serial.Serial(comport, baudrate=rate,
                                     stopbits=serial.STOPBITS_TWO,
                                     bytesize=serial.EIGHTBITS,
                                     timeout=0.5,
                                     parity=serial.PARITY_NONE)
        self._queries = ['DIA', 'RAT', 'VOL', 'TAR', 'VER']
        if not self.comLink.isOpen():
            self.comLink.open()
        # Empty the read cache
        _ = self.comLink.read(self.comLink.in_waiting)
        self.sendrcv('STP')
        self.logger.debug("Pump successful initialized")

    def sendrcv(self, command):
        """Takes a command and send it

        The string is converted to a compatible byte and send
        to the delay generator. The answer is analysed and in
        case a stanfordDG535Error is raised.

        :param command: Command string
        :type: string
        :returns: answer
        :rtype: string
        """
        hasanswer = False
        if command in self._queries:
            hasanswer = True
        sendbyte = bytearray(command, 'ASCII') + b'\r'
        if not self.comLink.isOpen():
            self.comLink.open()
        self.comLink.write(sendbyte)
        while not (self.comLink.out_waiting == 0):
            sleep(0.1)
        # small pause required to acknowledge input, different times due to response of pump
        if hasanswer:
            sleep(0.03)
        elif command in ['RUN', 'STP']:
            sleep(0.3)
        else:
            sleep(0.16)
        response = self.comLink.read(self.comLink.in_waiting)
        if not response:
            self.logger.error('No response from PHD2000 received')
            raise ValueError('No response from PHD2000 received')
        output = response.decode('ASCII')
        # if output[-1] == '*':
        #     self.logger.error('Error with message {} and command {}.'.format(output[-1], command))
        #     raise PHD2000Error(output[-1], command)
        if len(output) > 3:
            if output[3] == '?':
                self.logger.error('Error with message {} and command {}.'.format(output[3], command))
                raise PHD2000Error(output[3], command)
            elif output[3:6] == 'OOR':
                self.logger.error('Error with message {} and command {}.'.format(output[3:6], command))
                raise PHD2000Error(output[3:6], command)
        self.logger.debug("Command \"{}\" was issued.".format(command))
        if hasanswer:
            if command == "VER":
                return output[1:-4]
            else:
                return float(output[1:-4])

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if self.comLink.isOpen():
            self.sendrcv('STP')
            self.comLink.close()
