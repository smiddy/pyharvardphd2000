# pyHarvardPHD2000

Python SDK for the Harvard PHD 2000 Syringe Pump

Tested with
* Ubuntu 18.04.3 64bit
* Python 3.6.5 (Anaconda)
* pyserial 3.4

## Installation
Installation from project folder in current Python environment with

```python
python setup.py install
```

## Classes
The class phd2000.Device is the main class for interaction. Commands can be executed
with the `sendrcv` method. If command is a query, the function returns the answer as a string or float.

The class `phd2000.PHD2000Error` issues errors.

A context manager is availabe for safe operation. Please check `example.py` for an example. 
