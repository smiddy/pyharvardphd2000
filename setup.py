from setuptools import setup
from phd2000 import __version__


setup(
    name="phd2000",
    description="Python SDK for the Harvard PHD 2000 Syringe Pump",
    version=__version__,
    platforms=["any"],
    author="Markus J Schmidt",
    author_email='schmidt@ifd.mavt.ethz.ch',
    license="GNU GPLv3+",
    url="https://gitlab.ethz.ch/ifd-lab/device-driver/pyharvardphd2000",
    py_modules=["phd2000"],
    install_requires=['pyserial>=3.4'],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering',
        'License :: OSI Approved :: GNU General Public License v3 '
        'or later (GPLv3+)',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7'
    ],
    zip_safe=False
)
