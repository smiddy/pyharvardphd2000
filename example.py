import phd2000
import logging

port = '/dev/ttyUSB0'

# Create logger
logging.basicConfig(level=logging.DEBUG, filename='example.log',
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

with phd2000.Device(port) as pump:
    version = pump.sendrcv('VER')
    diametre = pump.sendrcv('DIA')
    pump.sendrcv('MMD30.0')
    pump.sendrcv('MLM5.1')
    pump.sendrcv('RUN')
    pump.sendrcv('STP')
pass
